package ru.andrey.catchUp;

/**
 * Демонстрационный класс {@code ThreadDemo}, который демонстрирует динамическое изменение приоритета потока
 * с целью устраивания догонялок между двумя потоками.
 */
public class ThreadDemo implements Runnable {
    Thread thread;
    private String message;
    private int maxValue;
    private int gapValue;
    private int delay;
    private int priority;

    /**
     * @param gapValue промежуток для изменения приоритета.
     * @param message  название потока.
     * @param maxValue максимальное значение цикла.
     * @param priority заданный приоритет потока.
     * @param delay    задержка вывода сообщения в консоль.
     */
    public ThreadDemo(String message, int maxValue, int gapValue, int priority, int delay) {
        this.priority = priority;
        this.gapValue = gapValue;
        this.message = message;
        this.maxValue = maxValue;
        this.delay = delay;
        thread = new Thread(this);
        thread.start();
    }

    /**
     * Возвращает {@code boolean} значение, проверяя на корректность заданного приоритета в диапозоне от 1 до 10.
     *
     * @param priority указанный приоритет.
     * @return {@code true} верно указан приоритет в заданном диапозоне от 1 до 10, иначе {@code false}.
     * @throws IllegalArgumentException если неверно указан приоритет, то устанавливается значение по умолчанию (5).
     */
    private boolean isCheckPriority(int priority) {
        try {
            thread.setPriority(priority);
        } catch (IllegalArgumentException e) {
            System.out.println("Неверный приоритет! " + e);
            thread.setPriority(5);
            return false;
        }
        return true;
    }

    @Override
    public void run() {
        // проверка на корректность ввода приоритета.
        if (!isCheckPriority(priority)) {
            System.out.println("Установлено значение по умолчанию (5)!");
        }
        System.out.println("Поток " + message + " запущен!");
        for (int i = maxValue; i > 0; i--) {
            if (i % gapValue == 0 && thread.getPriority() > 5) {
                thread.setPriority(1);
            } else if
                    (i % gapValue == 0 && thread.getPriority() <= 5) {
                thread.setPriority(10);
            }
            System.out.println(message + " " + i);
            try {
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                System.out.println("Поток " + message + " прерван!");
            }
        }
        System.out.println("Поток " + message + " завершён!");
    }
}
