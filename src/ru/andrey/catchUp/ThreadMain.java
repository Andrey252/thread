package ru.andrey.catchUp;

/**
 * В классе {@code ThreadMain} производится вызов два анонимных дочерних класса,
 * которые выводят сообщения для первого класса "first", а для второго "second" с целью
 * устраивания догонялок между двумя потоками.
 *
 * @author Андрей Рыжкин.
 * @since 09.11.2016
 */
public class ThreadMain {
    public static void main(String[] args) {
        new ThreadDemo("first", 999, 100, 1, 1);
        new ThreadDemo("second", 999, 100, 10, 1);
    }
}
