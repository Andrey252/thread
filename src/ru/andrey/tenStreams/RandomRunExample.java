package ru.andrey.tenStreams;


public class RandomRunExample extends Thread {
    public void run() {
        System.out.println(Thread.currentThread().getName());
    }

    /**
     * Метод запускает 10 потоков
     */
    public static void example() {
        for (int i = 0; i < 10; i++) {
            Thread thread = new RandomRunExample();
            //thread.setName(String.valueOf(i));
            thread.start();
        }
    }
}
