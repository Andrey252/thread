package ru.andrey.jointUseResource;

/**
 * Класс {@code ThreadMain} реализует запуск потока для инкрементирования переменной i (1 этап).
 * <p>
 * Создать класс, расширяющий класс Thread, для организации инкремента переменной i.
 * Создать два потока, выполняющих инкремент переменной i.
 * Вывести значение, которое должно получиться при совместном инкременте переменной i двумя
 * потоками. Например, если каждый поток увеличивает значение i 1000 раз, то в результате работы
 * двух потоков значение i должно увеличиться на 2000. Вывести значение, которое реально
 * получается при совместном инкременте переменной i двумя потоками. Проанализировать результат.
 * </p>
 *
 * @author Рыжкин Андрей
 * @version 1.1
 * @since 11.11.2016
 */
public class ThreadMain {

    public static void main(String[] args) {
        ThreadScore threadScore1 = new ThreadScore("1","testJointUseResource1.txt", 10000, 1, 1);
        ThreadScore threadScore2 = new ThreadScore("2","testJointUseResource2.txt", 10000, 1, 1);
        try {
            /* Метод join() ожидает завершение потоков.
            Подобно методу Thread.sleep() метод join может ждать в течение миллисекунд*/
            threadScore1.thread.join();
            threadScore2.thread.join();
        } catch (InterruptedException e) {
            System.out.println("Поток прерван!");
        }
        // возвращает true если threadScore1 и threadScore2 выполняется и false если поток еще не был запущен или был завершен.
            if (!(threadScore1.thread.isAlive() && threadScore2.thread.isAlive())) {
            System.out.println("Сумма двух потоков " + (threadScore1.getI() + threadScore2.getI()));
        }
    }
}
