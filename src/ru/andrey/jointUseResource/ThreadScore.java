package ru.andrey.jointUseResource;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Класс {@code ThreadScore}, который организует инкремент переменной i c заданным конечным значением и выводит значение
 * каждой итерации в консоль.
 *
 * @author Рыжкин Андрей.
 * @version 1.1
 * @since 11.11.2016
 */
public class ThreadScore extends Thread {
    Thread thread;
    private volatile static int i = 0;
    private String message;
    private int maxValue;
    private int priority;
    private int delay;
    private String pathFile;

    /**
     * @param message  название потока.
     * @param maxValue конечное значение завершения потока.
     * @param priority приоритет потока.
     * @param delay    задержка выполнения потока.
     */
    public ThreadScore(String message, String pathFile, int maxValue, int priority, int delay) {
        this.message = message;
        this.pathFile = pathFile;
        this.maxValue = maxValue;
        this.priority = priority;
        this.delay = delay;
        thread = new Thread(this);
        thread.start();
    }

    public static int getI() {
        return i;
    }

    /**
     * Возвращает {@code boolean} значение, проверяя на корректность заданного приоритета в диапозоне от 1 до 10.
     *
     * @param priority указанный приоритет.
     * @return {@code true} верно указан приоритет в заданном диапозоне от 1 до 10, иначе {@code false}.
     * @throws IllegalArgumentException если неверно указан приоритет, то устанавливается значение по умолчанию (5).
     */
    private boolean isCheckPriority(int priority) {
        try {
            setPriority(priority);
        } catch (IllegalArgumentException e) {
            System.out.println("Неверный приоритет! " + e + "\n");
            setPriority(5);
            return false;
        }
        return true;
    }


    /**
     * Метод записывает значение
     * каждой итерации переменной {@code i} в файл.
     *
     * @param pathFile путь записи файла.
     * @throws IOException если невозможно создать файл, неверно указан путь создания файла.
     */
    public synchronized void fileWriter(String pathFile) throws IOException {
        BufferedWriter outdata = new BufferedWriter(new FileWriter(pathFile));
        // проверка на корректность ввода приоритета.
        if (!isCheckPriority(priority)) {
            outdata.write("Установлено значение по умолчанию (5)!" + "\n");
            System.out.println("Установлено значение по умолчанию (5)!" + "\n");
        }
        outdata.write("Поток " + message + " запущен!" + "\n");
        System.out.println("Поток " + message + " запущен!");
        while (i < maxValue) {
            try {
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                outdata.write("Поток " + message + " прерван!" + "\n");
            }
            outdata.write("Поток " + message + ": " + i + "\n");
            increment();
        }
        outdata.write("Поток " + message + " завершён значением: " + i + "\n");
        System.out.println("Поток " + message + " завершён значением: " + i);
        outdata.close();
    }

    /**
     * Возвращает инкремент переменной {@code i}.
     *
     * @return {@code i++}.
     */
    public synchronized int increment() {
        return i++;
    }

    @Override
    public synchronized void run() {
        try {
            fileWriter(pathFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

