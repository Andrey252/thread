package ru.andrey.chikenOrTheEgg;

/**
 * Демонстрационный класс {@code ThreadDemo}, который запускает поток и выводит в
 * консоль название потока с указанным интервалом, приоритетом и задержки вывода названия потока в консоль.
 */
public class ThreadDemo implements Runnable {
    Thread thread;
    private int priority;
    private String message;
    private int length;
    private int delay;

    /**
     * @param priority приоритет потока.
     * @param message  название потока.
     * @param length   количество итераций цикла.
     * @param delay    задержка вывода названия потока в консоль.
     */
    public ThreadDemo(int priority, String message, int length, int delay) {
        this.priority = priority;
        this.message = message;
        this.length = length;
        this.delay = delay;
        thread = new Thread(this);
        thread.start();
    }

    /**
     * Возвращает {@code boolean} значение, проверяя на корректность заданного приоритета в диапозоне от 1 до 10.
     *
     * @param priority указанный приоритет.
     * @return {@code true} верно указан приоритет в заданном диапозоне от 1 до 10, иначе {@code false}.
     * @throws IllegalArgumentException если неверно указан приоритет, то устанавливается значение по умолчанию (5).
     */
    private boolean isCheckPriority(int priority) {
        try {
            thread.setPriority(priority);
        } catch (IllegalArgumentException e) {
            System.out.println("Неверный приоритет! " + e);
            thread.setPriority(5);
            return false;
        }
        return true;
    }

    @Override
    public void run() {
        // проверка на корректность ввода приоритета.
        if(!isCheckPriority(priority)){
            System.out.println("Установлено значение по умолчанию (5)!");
        }
        System.out.println("Поток " + message + " запущен!");
        for (int i = 0; i < length; i++) {
            System.out.println(message);
            try {
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                System.out.println("Поток " + message + " прерван!");
            }
        }
        System.out.println("Поток " + message + " завершён!");
    }
}
