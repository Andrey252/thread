package ru.andrey.chikenOrTheEgg;

/**
 * В классе {@code ThreadMain} производится вызов два анонимных дочерних класса,
 * которые выводят сообщения для первого класса "Яйцо", а для второго "Курица" с целью
 * спора: "Что появилось сначала - яйцо или курица?".Один поток должен выводить сообщение
 * "Курица", другой поток должен выводить сообщение "Яйцо". В результате работы приложения
 * должны выводиться результаты спора, в зависимости от того, какой поток сказал последнее слово.

 *
 * @author Андрей Рыжкин.
 * @since 09.11.2016
 */
public class ThreadMain {
    public static void main(String[] args) {
        ThreadDemo chicken = new ThreadDemo(2, "Курица", 100, 5);
        ThreadDemo egg = new ThreadDemo(1, "Яйцо", 100, 5);
        try {
            chicken.thread.join();
        } catch (InterruptedException e) {
        }
        if ((!chicken.thread.isAlive()) && egg.thread.isAlive()) {
            System.out.println("Победило Яйцо!");
        } else {
            System.out.println("Победила Курица!");
        }
    }
}